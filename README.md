# SIGNply SDK Android Demo

The source code for the SIGNply SDK Demo Application for Android operating systems is published here.

This app allows you to explore the integration options and full potential of the SIGNply SDK through a native Android app.

# Functioning

The application allows you to add several participants to sign a document.

It is necessary to add a participant and then select a PDF to be able to sign.

# Clarification

This application uses the SIGNply SDK library for the digital signature process that is located inside the app/libs folder.

The signing SDK receives a few parameters, including the document uri. The data model is exposed below

# Permissions

* Location: Used to add the geolocation to the signature if required.

* Network connection: Used in case you want to make a signature with a time stamp.


# Requirements

Operating system: Android 5.0 (Lollipop) or higher.

# Compilation

It can be compiled with Gradle or with Android Studio. For gradle use `gradle build`.

# Architecture

The application uses the architecture pattern [MVVM](https://developer.android.com/jetpack/guide?hl=es-419) (Model-View-ViewModel) recommended by Google.

* Material Design

* Jetpack Compose

* ViewModels

# License

The SIGNply Android SDK needs a license to work, which in this case is included in the Demo application that uses it (app/src/main/assets).

** This license is only valid for testing and should never be used in production.**

# Instantiation

The minimun instantiation process:

1 -> Declare a launcher with a new API

```kotlin

val getSignedDocument = rememberLauncherForActivityResult(contract = SignplySDKResultContract()) { result -> }
```

2 -> Declare a signplySDKParams with a minimun configuration

```kotlin
  val signplySDKParams = SignplySDKParams(
            licenseB64, //license in base 64
            SignplySDKFileProperties(uri.toString()) //uri path of the document
           ) 
        )
```

3 -> Launch the SDK using previous launcher

```kotlin
      try {
        getSignedDocument.launch(signplySDKParams)
      } catch (exception: Exception) {
        Log.e("SIGNplySDK", exception.message ?: "Unexpected error occurred")
      }
```

# Results

The result is obtained in the result of launcher (step 1), with an object of class SignplySDKIntentResult.

**SignplySDKIntentResult**
- file (Uri?): Signed file uri.
- image (Uri?): Signature image uri in jpg format.
- throwable (Throwable?): Error if any.

The throwable error can be of several types:
- SignplySDKInvalidLicenseException
- SignplySDKInvalidParamsException
- SignplySDKRenderPDFException
- SignplySDKSignPDFException

# Data Model

**SignplySDKParams**
- licenseB64 (String) : base 64 SIGNply license
- fileProperties (SignplySDKFileProperties): Contains file information
- commonProperties (SignplySDKCommonProperties): Contains common information
- widget (SignplySDKWidget): Configurable signature widget
- tsp (SignplySDKTSP): TSP configurable
- extra (SignplySDKExtra): Extra functionality in configurable SDK
- certificate (SignplySDKCertificate): Configurable signing certificate

**SignplySDKFileProperties**
- documentPath (String): String representation of Uri of pdf document
- signedName (String):  Signed document name. If not specified, by default it gets the value "signply_document.pdf". If the name already exists, a (1), (2), (3) are concatenated and so on.
- isShareable (Bool): If true, it shows a document share button
- password (String): Document password
- author (String): Signature author
- reason (String): Signature reason
- contact (String): Contact of the author or entity of the signature
- location (String): Location of the author or entity of the signature

**SignplySDKCommonProperties**
- title: (String): Toolbar title, if null, the name of the document is displayed.
- requestLocation (Bool): If true, ask the user for the location at the time of signing
- simulateTestingSignature (Boolean): For testing purposes. Adds an automatic signature when signing.
- saveOnPrivateStorage (Boolean): Indicates whether the document should be saved privately so that other apps cannot access the signed document getFilesDir() or otherwise to be accessible from other apps getExternalFilesDir().
  *Documents are deleted when the application is uninstalled. See more in
  https://developer.android.com/training/data-storage*
- renderDefaultIndexPage (Int): Indicates the page that you want to render directly. If it is null, it is positioned at the beginning of the document.
- showRecipientList (Boolean): Indicates if the list of participants is displayed from the Toolbar (by default true). There must be at least 2.
- captureSignatureButtonsTop (Boolean): Indicates if the canvas buttons "Accept" and "Delete" should be on top of canvas. Defaul is false.
- reference (String): External reference for grouping purposes. Default empty.
- saveSignatureImage (Boolean): If true, an image signature uri is returned along with the signed document.
- signatureImageMaxWidth (Int): Defines the maximum width of signature image returned (in px). Default is 320.
  *High values can throw an OutOfMemoryException handled by SDK that will return the original image*
- signatureImageMaxHeight (Int): Defines the maximum height of signature image returned (in px). Default is 160.
  *High values can throw an OutOfMemoryException handled by SDK that will return the original image*


**SIGNplySDKWidget**
- widgetType (SignplySDKWidgetType): Type of widget positioning. By default it uses the manual type.
- widgetFloatText (String):  Indicates the string to search within the document where the widget is embedded. Returns the first result obtained. This parameter is case sensitive.
- widgetFieldFieldName (String): name of the pre-existing field in which the widget is embedded.
- widgetManualRatio (Float): Widget width to height ratio for manual positioning. By default 2.5. The value must be between 1 and 4. If it is less than 1, it takes value 1 and if it is greater than 4 it takes value 4.
- widgetFixedPage (Int): Page number in which the widget is embedded. Starts at 1.
  *If the value is 0, sign on all pages*
  *If the value is greater than the total number of pages, it is signed on the last page*
- widgetFixedX (Int): Indicates the horizontal offset of the widget position from the bottom left corner of a document page.
- widgetFixedY (Int):  Indicates the vertical offset of the widget position from the bottom left corner of a document page.
- widgetFloatGapY (Int): Indicates the integer number of offset units (positive or negative) vertically from the bottom to the text
- widgetFloatGapX (Int):  Indicates the integer number of offset units (positive or negative) horizontally from the bottom to the text
- widgetCustomText (SignplySDKWidgetCustomText): Multiline array with the texts to embed in the signature widget
- widgetWidth (Int): Width of the widget. Minimum value 50. The default value is 150.
- widgetHeight (Int): Height of the widget. Minimum value 50. The default value 75.
- signOnAllPages (Boolean): Allows to sign on all pages.

**SignplySDKTSP**
- tspActivate (Bool): Time stamp input activation
- tspURL (String): TSP url
- tspUser (String):  TSP user
- tspPassword (String): TSP password

**SignplySDKExtra**
- autoOpen (Boolean): It allows to display the signature widget automatically when opening the document. (viewLastPage must be false).
- viewLastPage (Bool): If true, the user is required to view up to the last page in order to sign.
- signatureColorHex (String): Hexadecimal signature pencil color. By default ("# 000000") black.
- signatureThickness (Int): Signature pencil thickness. By default 10. It must be between 1 and 50.
- showReject (Bool): If true, show a reject document button
- captureSignatureTitle (String): Title text appearing in the signature box

**SignplySDKCertificate**
- signCertP12B64 (String): Base64 signing certificate.
- signCertPassword (String): Signing certificate password
- requestUserCertificate (Boolean): If true, prompts the user for a certificate at run time.
- defaultAliasUserCertiticate (String): If it asks for the certificate at runtime, select the alias of the default certificate that is passed to it in this variable.
- encKeyB64 (String): Public key for encryption of biometric data in base64
- ltv (Boolean): It allows to make a long-lasting signature.
  *If no signing certificate is specified, an internal one is used*

**SIGNplySDKWidgetType**
- Manual: Manual mode positioning in which the user can move the signature freely
- Field: Positioning looking for a specific signature field
- Fixed: Fixed positioning in the document, it is passed a page number and a relative position within the document
- Float: Floating positioning in the document based on a text string search

**SignplySDKWidgetCustomText**
- fontSize (Int): Text size
- text (String): Text string


# Theming

The SDK uses a custom Theme that inherits from Theme.MaterialComponents.DayNight.DarkActionBar.
These can be overwritten from the main application.
https://developer.android.com/guide/topics/ui/look-and-feel/themes
https://material.io/develop/android/docs/getting-started

If you want to override the theme, you can always apply a style for an activity as follows:

In the AndroidManifest.xml:
```xml
<activity          
android:name="com.edatalia.signplysdk.presentation.SignplySDKLauncher"
android:theme="@style/Theme.CustomTheme" />
```

In themes.xml inside values:
```xml
<style name="Theme.CustomTheme" parent="Theme.MaterialComponents.DayNight.DarkActionBar">
        <!-- Primary brand color. -->
        <item name="colorPrimary">#0057A0</item>
        <item name="colorPrimaryVariant">#002F71</item>
        <item name="colorOnPrimary">@android:color/white</item>
        <!-- Secondary brand color. -->
        <item name="colorSecondary">#0057A0</item>
        <item name="colorSecondaryVariant">#002F71</item>
        <item name="colorOnSecondary">@android:color/white</item>
        <!-- Status bar color. -->
</style>
```

The same operation could be done for the folder dark mode (values-night) by choosing other colors. This can be seen in the demo project.

# Dependencies

Within the build.gradle project dependencies (app module) you need
add the following lines and sync the project.
```groovy
implementation 'androidx.appcompat:appcompat:1.4.0' //SDK
implementation 'androidx.core:core-ktx:1.7.0' //SDK
implementation 'com.google.android.material:material:1.4.0' //SDK
implementation 'androidx.constraintlayout:constraintlayout:2.1.2' //SDK
implementation "org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2" //SDK
implementation files('libs/*aar_name*')
```
** aar_name is the name of the aar signply sdk library in /libs

# Annotations

Documents that have been signed and are not private can be viewed from the application folder. For this it is necessary to access the following folder: Within the Storage it is necessary to go to Android -> data -> (application identifier) -> files
In this folder are all the PDF files that have been signed from the SIGNply SDK.

# FAQ

**How to get a Uri with permissions from a File**

1 -> Specify the FileProvider in the XML manifest

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="{name_of_your_app_package}">
    <application
        ...>
        <provider
        android:name="androidx.core.content.FileProvider"
        android:authorities="{package_name}.fileprovider"
        android:exported="false"
        android:grantUriPermissions="true">
        <meta-data
            android:name="android.support.FILE_PROVIDER_PATHS"
            android:resource="@xml/filepaths" />
        </provider>
        ...
    </application>
</manifest>
```


2 -> Create filepaths.xml file inside res/xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<paths>
    <external-path name="external" path="." />
    <external-files-path name="external_files" path="." />
    <cache-path name="cache" path="." />
    <external-cache-path name="external_cache" path="." />
    <files-path name="files" path="." />
</paths>
```

3 -> In code use that FileProvider:
```kotlin
 Uri uri = FileProvider.getUriForFile(context,
 {package_name}.fileprovider, file)
 ```

------------

**How to lock device orientation in signature**

The solution you can do is to add the following in your AndroidManifest.xml
In this way the orientation is forced in the entire activity. Not only when signing.

To force vertical:
```xml
<activity android:name="com.edatalia.signplysdk.presentation.SignplySDKLauncher"
        android:screenOrientation="portrait"
        tools:replace="screenOrientation">
```

To force horizontal:
```xml
<activity android:name="com.edatalia.signplysdk.presentation.SignplySDKLauncher"
        android:screenOrientation="landscape"
        tools:replace="screenOrientation">
```

------------


**SDK colors or components (buttons, text) are not displayed correctly**

Review theming section
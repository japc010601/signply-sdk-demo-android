package com.edatalia.signplysdk.demo.presentation.util

import android.net.Uri

sealed class UiEvent {
    data class LaunchSIGNplySDK(val uri: Uri): UiEvent()
}
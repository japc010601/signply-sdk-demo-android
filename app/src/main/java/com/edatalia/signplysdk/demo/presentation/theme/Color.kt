package com.edatalia.signplysdk.demo.presentation.theme

import androidx.compose.ui.graphics.Color

val Signply200 = Color(0xFF608ED6)
val Signply500 = Color(0xFF0057A0)
val Signply700 = Color(0xFF002F71)
val Gray900 = Color(0xFF121212)
val Gray200 = Color(0xFFF5F5F5)
package com.edatalia.signplysdk.demo.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.edatalia.signplysdk.demo.presentation.screen.config.ConfigScreen
import com.edatalia.signplysdk.demo.presentation.screen.config.ConfigViewModel

@ExperimentalComposeUiApi
@Composable
fun Navigation() {
    val navController = rememberNavController()

    val configViewModel: ConfigViewModel = hiltViewModel()

    NavHost(navController = navController, startDestination = Screen.ConfigScreen.route) {
        composable(Screen.ConfigScreen.route) {
            ConfigScreen(navController, configViewModel)
        }
    }
}
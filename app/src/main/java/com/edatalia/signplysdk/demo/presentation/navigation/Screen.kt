package com.edatalia.signplysdk.demo.presentation.navigation

sealed class Screen(val route: String) {
    object ConfigScreen: Screen("config_screen")
}
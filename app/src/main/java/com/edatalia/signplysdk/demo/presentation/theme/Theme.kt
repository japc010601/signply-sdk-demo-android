package com.edatalia.signplysdk.demo.presentation.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    primary = Signply200,
    primaryVariant = Signply700,
    secondary = Signply200,
    onBackground = Gray200,
    background = Gray900
)

private val LightColorPalette = lightColors(
    primary = Signply500,
    primaryVariant = Signply700,
    secondary = Signply500,
    onBackground = Gray900
)

@Composable
fun UseCase_JetpackComposeTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = appTypography,
        shapes = Shapes,
        content = content
    )
}
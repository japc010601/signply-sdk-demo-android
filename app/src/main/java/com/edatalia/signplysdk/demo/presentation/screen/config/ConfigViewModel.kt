package com.edatalia.signplysdk.demo.presentation.screen.config

import android.content.Context
import android.net.Uri
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edatalia.signplysdk.data.*
import com.edatalia.signplysdk.data.SIGNplySDKConstants.DEFAULT_NAME_SIGNED_DOCUMENT
import com.edatalia.signplysdk.demo.R
import com.edatalia.signplysdk.demo.presentation.util.UiEvent
import com.edatalia.signplysdk.demo.util.Constants
import com.edatalia.signplysdk.demo.util.Constants.DEFAULT_APP_BAR_TITLE
import com.edatalia.signplysdk.demo.util.Constants.LICENSE_FILENAME
import com.edatalia.signplysdk.demo.util.Constants.SIGNATURE_AUTHOR
import com.edatalia.signplysdk.demo.util.Constants.SIGNATURE_CONTACT
import com.edatalia.signplysdk.demo.util.Constants.SIGNATURE_LOCATION
import com.edatalia.signplysdk.demo.util.Constants.SIGNATURE_REASON
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.io.BufferedReader
import javax.inject.Inject

@HiltViewModel
class ConfigViewModel @Inject constructor() : ViewModel() {

    private val _toolbarTitle = mutableStateOf(DEFAULT_APP_BAR_TITLE)
    val toolbarTitle: State<String> = _toolbarTitle

    private val _defaultRenderPage: MutableState<Int?> = mutableStateOf(1)
    val defaultRenderPage: State<Int?> = _defaultRenderPage

    private val _documentSignedName: MutableState<String> = mutableStateOf(DEFAULT_NAME_SIGNED_DOCUMENT)
    val documentSignedName: State<String> = _documentSignedName

    private val _documentPassword: MutableState<String> = mutableStateOf("")
    val documentPassword: State<String> = _documentPassword

    private val _documentPasswordVisibility = mutableStateOf(false)
    val documentPasswordVisibility: State<Boolean> = _documentPasswordVisibility

    private val _documentShareable = mutableStateOf(false)
    val documentShareable: State<Boolean> = _documentShareable

    private val _documentPrivate = mutableStateOf(false)
    val documentPrivate: State<Boolean> = _documentPrivate

    private val _author = mutableStateOf(SIGNATURE_AUTHOR)
    val author: State<String> = _author

    private val _reason = mutableStateOf(SIGNATURE_REASON)
    val reason: State<String> = _reason

    private val _contact = mutableStateOf(SIGNATURE_CONTACT)
    val contact: State<String> = _contact

    private val _location = mutableStateOf(SIGNATURE_LOCATION)
    val location: State<String> = _location

    private val _requestUserLocation: MutableState<Boolean> = mutableStateOf(false)
    val requestUserLocation: State<Boolean> = _requestUserLocation


    private val _topSignatureButtons: MutableState<Boolean> = mutableStateOf(false)
    val topSignatureButtons: State<Boolean> = _topSignatureButtons


    private val _uiEvent =  Channel<UiEvent>()
    val uiEvent = _uiEvent.receiveAsFlow()

    fun setToolbarTitle(value: String) {
        _toolbarTitle.value = value
    }

    fun setDefaultRenderPage(value: String) {
        _defaultRenderPage.value = value.toIntOrNull()
    }

    fun setDocumentPassword(value: String) {
        _documentPassword.value = value
    }

    fun setDocumentShareable(value: Boolean) {
        _documentShareable.value = value
    }

    fun setDocumentPrivate(value: Boolean) {
        _documentPrivate.value = value
    }

    fun toggleDocumentPasswordVisibility() {
        _documentPasswordVisibility.value = !_documentPasswordVisibility.value
    }

    fun setDocumentSignedName(value: String) {
        _documentSignedName.value = value
    }

    fun setAuthor(value: String) {
        _author.value = value
    }

    fun setReason(value: String) {
        _reason.value = value
    }

    fun setContact(value: String) {
        _contact.value = value
    }

    fun setLocation(value: String) {
        _location.value = value
    }

    fun setRequestUserLocation(value: Boolean) {
        _requestUserLocation.value = value
    }

    fun setTopSignatureButtons(value: Boolean) {
        _topSignatureButtons.value = value
    }

    private fun getLicenseB64(context: Context): String {
        return context
            .assets
            .open(LICENSE_FILENAME)
            .bufferedReader()
            .use(BufferedReader::readText)
    }

    fun getSignplySDKParams(uri: Uri, context: Context) : SignplySDKParams {

        val licenseB64 = getLicenseB64(context)
        val signplySDKParams = SignplySDKParams(
            licenseB64,
            SignplySDKFileProperties(uri.toString()),
            SignplySDKCommonProperties()
        )

        signplySDKParams.commonProperties.title = _toolbarTitle.value
        signplySDKParams.commonProperties.renderDefaultIndexPage =
            defaultRenderPage.value?.minus(1) ?: SIGNplySDKConstants.DEFAULT_RENDER_INDEX_PAGE
        signplySDKParams.commonProperties.saveOnPrivateStorage = _documentPrivate.value
        signplySDKParams.commonProperties.requestLocation = _requestUserLocation.value
        signplySDKParams.commonProperties.captureSignatureButtonsTop = _topSignatureButtons.value

        signplySDKParams.fileProperties.password = _documentPassword.value
        signplySDKParams.fileProperties.isShareable = _documentShareable.value
        signplySDKParams.fileProperties.signedName = documentSignedName.value
        signplySDKParams.fileProperties.author = _author.value
        signplySDKParams.fileProperties.reason = _reason.value
        signplySDKParams.fileProperties.contact = _contact.value
        signplySDKParams.fileProperties.location = _location.value

        /** WIDGET **/
        val widget = SignplySDKWidget(_widgetType.value)

        when (widget.widgetType) {
            SignplySDKWidgetType.Manual -> {
                widget.widgetManualRatio = _widgetManualRatio.value
            }
            SignplySDKWidgetType.Fixed -> {
                widget.widgetFixedPage = _widgetFixedPage.value
                widget.widgetFixedX = _widgetFixedX.value
                widget.widgetFixedY = _widgetFixedY.value
                widget.widgetWidth = _widgetFixedWidth.value ?: SIGNplySDKConstants.DEFAULT_FIXED_WIDGET_WIDTH
                widget.widgetHeight = _widgetFixedHeight.value ?: SIGNplySDKConstants.DEFAULT_FIXED_WIDGET_HEIGHT
            }
            SignplySDKWidgetType.Float -> {
                widget.widgetFloatText = widgetFloatText.value
                widget.widgetWidth = _widgetFixedWidth.value ?: SIGNplySDKConstants.DEFAULT_FIXED_WIDGET_WIDTH
                widget.widgetHeight = _widgetFixedHeight.value ?: SIGNplySDKConstants.DEFAULT_FIXED_WIDGET_HEIGHT
                widget.widgetFloatGapX = _widgetFieldGapX.value
                widget.widgetFloatGapY = _widgetFieldGapY.value
            }
            SignplySDKWidgetType.Field -> {
                widget.widgetFieldFieldName = _widgetFieldName.value
            }
        }

        widget.requestWidgetCustomText = _requestWidgetCustomText.value
        widget.signOnAllPages = _signatureAllPages.value

        val customTextList = listOf(
            SignplySDKWidgetCustomText(
                _widgetCustomText.value,
                widgetCustomTextFontSize.value ?: SIGNplySDKConstants.DEFAULT_CUSTOM_TEXT_FONT_SIZE
            )
        )
        widget.widgetCustomText = customTextList

        signplySDKParams.widget = widget

        /** TSP **/
        if (advancedSignature.value) {
            signplySDKParams.tsp.tspActivate = true
            signplySDKParams.tsp.tspURL = _tspUrl.value
            signplySDKParams.tsp.tspPassword = _tspPassword.value
            signplySDKParams.tsp.tspUser = _tspUser.value
        }



        /** CERTIFICATE **/
        signplySDKParams.certificate.requestUserCertificate = _requestUserCertificate.value
        signplySDKParams.certificate.ltv = _ltv.value

        /** EXTRA **/
        signplySDKParams.extra.autoOpen = _autoOpen.value
        signplySDKParams.extra.viewLastPage = _viewLastPage.value
        signplySDKParams.extra.captureSignatureTitle = _captureSignatureTitle.value

        signplySDKParams.extra.signatureColorHex = getSelectedSignatureColorHex()
        signplySDKParams.extra.signatureThickness = _signatureThickness.value

        return signplySDKParams
    }

    fun launchSIGNplySDK(uri: Uri) {
        sendUiEvent(event = UiEvent.LaunchSIGNplySDK(uri))
    }

    private fun sendUiEvent(event: UiEvent) {
        viewModelScope.launch {
            _uiEvent.send(event)
        }
    }

    //ADVANCED

    private val _captureSignatureTitle = mutableStateOf("Sign here")
    val captureSignatureTitle: State<String> = _captureSignatureTitle

    private val _widgetType = mutableStateOf(SignplySDKWidgetType.Manual)
    val widgetType: State<SignplySDKWidgetType> = _widgetType

    private val _widgetFieldName = mutableStateOf("")
    val widgetFieldName: State<String> = _widgetFieldName

    private val _widgetFloatText = mutableStateOf("")
    val widgetFloatText: State<String> = _widgetFloatText

    private val _widgetManualRatio: MutableState<Float> = mutableStateOf(SIGNplySDKConstants.DEFAULT_MANUAL_WIDGET_RATIO)
    val widgetManualRatio: State<Float> = _widgetManualRatio

    private val _widgetFixedPage: MutableState<Int?> = mutableStateOf(1)
    val widgetFixedPage: State<Int?> = _widgetFixedPage

    private val _widgetFixedX: MutableState<Int?> = mutableStateOf(0)
    val widgetFixedX: State<Int?> = _widgetFixedX

    private val _widgetFixedY: MutableState<Int?> = mutableStateOf(0)
    val widgetFixedY: State<Int?> = _widgetFixedY

    private val _widgetFixedHeight: MutableState<Int?> = mutableStateOf(SIGNplySDKConstants.DEFAULT_FIXED_WIDGET_HEIGHT)
    val widgetFixedHeight: State<Int?> = _widgetFixedHeight

    private val _widgetFixedWidth: MutableState<Int?> = mutableStateOf(SIGNplySDKConstants.DEFAULT_FIXED_WIDGET_WIDTH)
    val widgetFixedWidth: State<Int?> = _widgetFixedWidth

    private val _widgetFieldGapX: MutableState<Int?> = mutableStateOf(0)
    val widgetFieldGapX: State<Int?> = _widgetFieldGapX

    private val _widgetFieldGapY: MutableState<Int?> = mutableStateOf(0)
    val widgetFieldGapY: State<Int?> = _widgetFieldGapY

    private val _widgetCustomText: MutableState<String> = mutableStateOf("")
    val widgetCustomText: State<String> = _widgetCustomText

    private val _widgetCustomTextFontSize: MutableState<Int?> = mutableStateOf(
        SIGNplySDKConstants.DEFAULT_CUSTOM_TEXT_FONT_SIZE
    )

    val widgetCustomTextFontSize: State<Int?> = _widgetCustomTextFontSize

    private val _advancedSignature: MutableState<Boolean> = mutableStateOf(false)
    val advancedSignature: State<Boolean> = _advancedSignature

    private val _tspUrl: MutableState<String> = mutableStateOf(Constants.DEFAULT_TSP_URL)
    val tspUrl: State<String> = _tspUrl

    private val _tspUser: MutableState<String> = mutableStateOf("")
    val tspUser: State<String> = _tspUser

    private val _tspPassword: MutableState<String> = mutableStateOf("")
    val tspPassword: State<String> = _tspPassword

    private val _tspPasswordVisibility: MutableState<Boolean> = mutableStateOf(false)
    val tspPasswordVisibility = _tspPasswordVisibility

    val widgetTypeValues = SignplySDKWidgetType.values()

    val signatureTextColorValues = SignatureTextColor.values()

    private val _signatureTextColor = mutableStateOf(SignatureTextColor.BLACK)
    val signatureTextColor: State<SignatureTextColor> = _signatureTextColor

    private val _autoOpen: MutableState<Boolean> = mutableStateOf(false)
    val autoOpen: State<Boolean> = _autoOpen

    private val _viewLastPage: MutableState<Boolean> = mutableStateOf(false)
    val viewLastPage: State<Boolean> = _viewLastPage

    private val _requestUserCertificate: MutableState<Boolean> = mutableStateOf(false)
    val requestUserCertificate: State<Boolean> = _requestUserCertificate

    private val _requestWidgetCustomText: MutableState<Boolean> = mutableStateOf(false)
    val requestWidgetCustomText: State<Boolean> = _requestWidgetCustomText

    private val _ltv: MutableState<Boolean> = mutableStateOf(false)
    val ltv: State<Boolean> = _ltv

    private val _signatureThickness: MutableState<Int> = mutableStateOf(SIGNplySDKConstants.DEFAULT_SIGNATURE_THICKNESS)
    val signatureThickness: State<Int> = _signatureThickness

    private val _signatureAllPages: MutableState<Boolean> = mutableStateOf(false)
    val signatureAllPages: State<Boolean> = _signatureAllPages
    fun setCaptureSignatureTitle(value: String) {
        _captureSignatureTitle.value = value
    }

    fun setSignatureOnAllPages(value: Boolean) {
        _signatureAllPages.value = value
    }

    fun setWidgetType(value: SignplySDKWidgetType) {
        _widgetType.value = value
    }

    fun setSignatureTextColor(value: SignatureTextColor) {
        _signatureTextColor.value = value
    }

    fun setSignatureThickness(value: Int) {
        _signatureThickness.value = value
    }

    fun setWidgetManualRatio(value: Float) {
        _widgetManualRatio.value = value
    }

    fun setWidgetFixedX(value: Int?) {
        _widgetFixedX.value = value
    }

    fun setWidgetFixedY(value: Int?) {
        _widgetFixedY.value = value
    }

    fun setWidgetFixedHeight(value: Int?) {
        _widgetFixedHeight.value = value
    }

    fun setWidgetFixedWidth(value: Int?) {
        _widgetFixedWidth.value = value
    }

    fun setWidgetFixedPage(value: Int?) {
        _widgetFixedPage.value = value
    }

    fun setWidgetFieldName(value: String) {
        _widgetFieldName.value = value
    }

    fun setWidgetFloatText(value: String) {
        _widgetFloatText.value = value
    }

    fun setWidgetFieldGapX(value: Int?) {
        _widgetFieldGapX.value = value
    }

    fun setWidgetFieldGapY(value: Int?) {
        _widgetFieldGapY.value = value
    }

    fun setWidgetCustomText(value: String) {
        _widgetCustomText.value = value
    }

    fun setWidgetCustomTextFontSize(value: String) {
        _widgetCustomTextFontSize.value = value.toIntOrNull()
    }

    fun setAdvancedSignature(value: Boolean) {
        _advancedSignature.value = value
    }

    fun setTspUrl(value: String) {
        _tspUrl.value = value
    }

    fun setTspUsername(value: String) {
        _tspUser.value = value
    }

    fun setTspPassword(value: String) {
        _tspPassword.value = value
    }

    fun toggleTspPasswordVisibility() {
        _tspPasswordVisibility.value = !_tspPasswordVisibility.value
    }

    fun setAutoOpen(value: Boolean) {
        _autoOpen.value = value
    }

    fun setViewLastPage(value: Boolean) {
        _viewLastPage.value = value
    }

    fun setRequestUserCertificate(value: Boolean) {
        _requestUserCertificate.value = value
    }

    fun setRequestWidgetCustomText(value: Boolean) {
        _requestWidgetCustomText.value = value
    }

    fun setLTV(value: Boolean) {
        _ltv.value = value
    }

    fun getDisplayNameForWidget(widgetType: SignplySDKWidgetType, context: Context) : String {
        return when (widgetType) {
            SignplySDKWidgetType.Field -> {
                context.getString(R.string.field)
            }
            SignplySDKWidgetType.Manual -> {
                context.getString(R.string.manual)
            }
            SignplySDKWidgetType.Float -> {
                context.getString(R.string.text)
            }
            SignplySDKWidgetType.Fixed -> {
                context.getString(R.string.fixed)
            }
        }
    }

    fun getDisplayNameForSignatureColor(signatureTextColor: SignatureTextColor, context: Context) : String {
        return when (signatureTextColor) {
            SignatureTextColor.BLACK -> {
                context.getString(R.string.black)
            }
            SignatureTextColor.GRAY -> {
                context.getString(R.string.gray)
            }
            SignatureTextColor.BLUE -> {
                context.getString(R.string.blue)
            }
        }
    }
    private fun getSelectedSignatureColorHex() : String {
        return when (_signatureTextColor.value) {
            SignatureTextColor.BLACK -> {
                "#000000"
            }
            SignatureTextColor.GRAY -> {
                "#555555"
            }
            SignatureTextColor.BLUE -> {
                "#002F71"
            }
        }
    }
}

enum class SignatureTextColor {
    BLACK, GRAY, BLUE
}